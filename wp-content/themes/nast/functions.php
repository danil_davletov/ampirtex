<?php

$my_theme = wp_get_theme();

define( 'PGL_THEME_VERSION', $my_theme->get( 'Version' ) );



require( 'framework/init.php');

//custom translations
load_theme_textdomain( 'greatshop', get_template_directory().'/languages' );
load_theme_textdomain( 'woocommerce', get_template_directory().'/languages' );

add_filter('woocommerce_variable_price_html', 'custom_variation_price', 10, 2);

function custom_variation_price( $price, $product ) {

$price = '';

if ( !$product->min_variation_price || $product->min_variation_price !== $product->max_variation_price ) $price .= '' . _x( ' ', 'min_price', 'woocommerce') . ' ';

$price .= woocommerce_price($product->get_price());

return $price;

}

/**
 *	Testimonials
 */
 function register_custom_post() {
  $labels = array(
    'name' => _x('Отзывы', 'post type general name'),
    'singular_name' => _x('Отзыв', 'post type singular name'),
    'add_new' => _x('Добавить', 'Отзыв'),
    'add_new_item' => __('Добавить Отзыв'),
    'edit_item' => __('Редактировать Отзыв'),
    'new_item' => __('Новый Отзыв'),
    'all_items' => __('Все Отзывы'),
    'view_item' => __('Просмотреть Отзыв'),
    'search_items' => __('Искать Отзывы'),
    'not_found' =>  __('Ничего не найдено'),
    'not_found_in_trash' => __('Ничего не найдено в корзине'), 
    'parent_item_colon' => '',
    'menu_name' => __('Отзывы')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'thumbnail' ,'page-attributes' ),
    // 'taxonomies' => array( 'category' )
  ); 
  register_post_type('testimonial',$args);
}
add_action( 'init', 'register_custom_post' );


/**
*	Testimonials shortcode [testimonials delay="5" category="homepage"]
*/
function testimonials_shortcode( $atts ) {

	$delay      = is_int($atts['delay']) ?  $atts['delay'] * 1000  : 6000;
	//$categories = !empty($atts['category']) ? $atts['category'] : '';

	$args = array( 
		'post_type' => 'testimonial', 
		'posts_per_page' => 30,
		'orderby' => 'menu_order title',
		'order'   => 'ASC',
		//'category_name' => $categories
	);
	$testimonials_query = new WP_Query( $args ); 

	if ( $testimonials_query->have_posts() ) : 

		$output.='
		<div class="testimonials_container" style="">
			<div class="row">';
			$i=0;
			while ( $testimonials_query->have_posts() ) : 
				$testimonials_query->the_post();
				
				if (has_post_thumbnail()) {
				 	$t_thumbnail = '<div class="testimonial-thumbnail">'.get_the_post_thumbnail(null,'thumbnail').'</div>'; 
				 	$item_class = '';
				} else {
					$t_thumbnail =  '<div class="testimonial-thumbnail">
					<img src="http://ampirtex37.ru/wp-content/uploads/2017/07/bg_blank_testimonial.png" alt=""></div>'; 
					$item_class = 'no-image-testimonial';
				}
				if (($i % 2 == 0) && ($i > 1) )
				$output.='</div><div class="row">';
				$output.='
				<div class="col-sm-6">
				<div class="item '.$item_class.'" >
					'.$t_thumbnail.'
					<div class="testimonial-content">
						<div class="testimonial-text">
							'.get_the_content().'
						</div>
						<div class="testimonial-author">
						<p class="author-name">
							<b>'.get_the_title().'</b>
						</p>
						</div>
					</div><!--/testimonial-content-->
				</div><!--/item-->
				</div><!--/col-->';
				$i++;
			endwhile;

		$output.='</div> <!--/row-->
		</div>';


		// $output.='';

		wp_reset_postdata();

	else:  
		$output.='<p>Sorry, no testimonials found.</p>';
	endif; 


	return $output;
	//return "num = {$atts['num']}";
}
add_shortcode( 'testimonials', 'testimonials_shortcode' );





/**
 * Enqueue scripts and styles.
 */

function amp37_scripts_styles() {

	

	// updates

	wp_enqueue_style( 'updates', get_template_directory_uri(). '/updates.css' , false, '1.0.36' );

	wp_enqueue_script( 'my-script', get_template_directory_uri() . '/js/re-scripts.js', array('jquery'), '1.0.17', true );

	// fancybox
	wp_enqueue_style( 'fancybox.css', get_template_directory_uri(). '/css/jquery.fancybox.min.css' , false, '1.0.0' );
	wp_enqueue_script( 'fancybox.js', get_template_directory_uri() . '/js/jquery.fancybox.min.js', array('jquery'), '1.0.0', true );



}

add_action( 'wp_enqueue_scripts', 'amp37_scripts_styles' );




// function show_min_total_price(){

// 	echo '<p class="min-total-in-cart"><b>Минимальная сумма заказа - 7 000 руб.</b></p>';

// }
// add_action('woocommerce_cart_totals_after_order_total', 'show_min_total_price');

function pagination_on_top() {
	echo '<div class="ampirtex_top_pagination">';
	woocommerce_pagination();//wc_get_template( 'loop/pagination.php' );
	echo '</div>';
}

add_action( 'woocommerce_before_shop_loop', 'pagination_on_top', 999 );



add_action( 'woocommerce_check_cart_items', 'wc_minimum_order_amount' );
function wc_minimum_order_amount() {
	global $woocommerce;
	$minimum = 7000;
	if ( $woocommerce->cart->total < $minimum ) {
		wc_add_notice( 'Минимальная сумма заказа - '.$minimum.' руб.' ,	'error'	);
	}
}


/*Woocommerce sort items in order*/
function compareItemByName($a, $b) {
	return strcasecmp($a['name'],$b['name']);
}
function sortOrderItemsByName($items, $order) {
	usort($items, 'compareItemByName');
	return $items;
} 
add_filter('woocommerce_order_get_items', 'sortOrderItemsByName', 10, 2);

