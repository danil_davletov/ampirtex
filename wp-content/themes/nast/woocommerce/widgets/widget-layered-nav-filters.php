<?php
/**
 * Layered Navigation Fitlers Widget
 *
 * @author 		WooThemes
 * @category 	Widgets
 * @package 	WooCommerce/Widgets
 * @version 	2.0.1
 * @extends 	WC_Widget
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class PGL_Widget_Layered_Nav_Filters extends WC_Widget_Layered_Nav_Filters {
}

register_widget( 'PGL_Widget_Layered_Nav_Filters' );