<?php
/**
 * Edit address form
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $current_user,$woocommerce, $yith_wcwl;

$page_title = ( $load_address === 'billing' ) ? __( 'Billing Address', 'woocommerce' ) : __( 'Shipping Address', 'woocommerce' );

get_currentuserinfo();

?>

<?php wc_print_notices(); ?>
<div class="row">
	<div class="my-account-left col-sm-3">
		<div class="list-group my-account-group">
			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#my-orders" class="list-group-item"><?php _e("My Orders", 'greatshop'); ?></a>
			<?php if ( $downloads = WC()->customer->get_downloadable_products() ) { ?>
				<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#my-downloads" class="list-group-item"><?php _e("My Downloads", 'greatshop'); ?></a>
			<?php } ?>
			
			<?php if ( class_exists( 'YITH_WCWL_UI' ) ) { ?>
			<a href="<?php echo $yith_wcwl->get_wishlist_url(); ?>" class="list-group-item"><?php _e("My Wishlist", 'greatshop'); ?></a>
			<?php } ?>
			<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>#address-book" class="list-group-item"><?php _e("Address Book", 'greatshop'); ?></a>
			
			<a href="<?php echo wc_customer_edit_account_url(); ?>" class="active list-group-item"><?php _e("Change Password", 'greatshop'); ?></a>
			
			<a href="<?php echo wp_logout_url(site_url()); ?>" class="list-group-item"><?php _e('Logout','greatshop') ?></a>
		</div>
	</div>
	<div class="col-sm-9">
		<?php if ( ! $load_address ) : ?>

			<?php wc_get_template( 'myaccount/my-address.php' ); ?>

		<?php else : ?>

			<form method="post">

				<h3 class="widget-title"><?php echo apply_filters( 'woocommerce_my_account_edit_address_title', $page_title ); ?></h3>

				<?php do_action( "woocommerce_before_edit_address_form_{$load_address}" ); ?>

				<?php foreach ( $address as $key => $field ) : ?>

					<?php woocommerce_form_field( $key, $field, ! empty( $_POST[ $key ] ) ? wc_clean( $_POST[ $key ] ) : $field['value'] ); ?>

				<?php endforeach; ?>

				<?php do_action( "woocommerce_after_edit_address_form_{$load_address}" ); ?>

				<p>
					<input type="submit" class="btn btn-primary" name="save_address" value="<?php _e( 'Save Address', 'woocommerce' ); ?>" />
					<?php wp_nonce_field( 'woocommerce-edit_address' ); ?>
					<input type="hidden" name="action" value="edit_address" />
				</p>

			</form>

		<?php endif; ?>
	</div>
</div>
