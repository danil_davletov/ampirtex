<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you (the theme developer).
 * will need to copy the new files to your theme to maintain compatibility. We try to do this.
 * as little as possible, but it does happen. When this occurs the version of the template file will.
 * be bumped and the readme will list any important changes.
 *
 * @see 	    http://docs.woothemes.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}


if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
    $bg = 'background-image: url("'. $img .'"); background-repeat: no-repeat;';
} else $bg = '';



?>
<!DOCTYPE html>
<html dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	</head>
    <body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
    	<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'?>">
        	<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
            	<tr>
                	<td align="center" valign="top">

                    	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_container">
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Header -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style='<?= $bg ?>'>
                                        <tr>
                                            <td id="header_wrapper">
                                            	<h1><?php echo $email_heading; ?></h1>
                                            </td>
                                        </tr>
                                    </table><!-- End Header --></td>
                            </tr>
                            <tr>
                                <td align="center" valign="top">
                                    <table width="600" height="53" bgcolor="#fedead" align="center" style="text-align:center;color: #202020; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif; font-weight: 700; "><tbody><tr><td width="120" height="44"><strong> <span style="color:rgb(255, 255, 255);font-size:12pt;"><a href="http://ampirtex37.ru/cat/kpb1/" style="text-decoration:none;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:rgb(0, 0, 0);" rel="noopener noreferrer" target="_blank">КПБ</a></span></strong></td><td width="120" height="44"><strong><span style="color:rgb(255, 255, 255);font-size:12pt;"><a href="http://ampirtex37.ru/cat/odeyala/" style="text-decoration:none;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:rgb(0, 0, 0);" rel="noopener noreferrer" target="_blank">Одеяла</a></span></strong></td><td width="120" height="44"><strong><span style="color:rgb(255, 255, 255);font-size:12pt;"><a href="http://ampirtex37.ru/cat/pokrivala/" style="text-decoration:none;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:rgb(0, 0, 0);" rel="noopener noreferrer" target="_blank">Покрывала</a></span></strong></td><td width="120" height="44"><strong><span style="color:rgb(255, 255, 255);font-size:12pt;"><a href="http://ampirtex37.ru/dostavka-i-oplata/" style="text-decoration:none;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:rgb(0, 0, 0);" rel="noopener noreferrer" target="_blank">Доставка</a></span></strong></td><td width="120" height="44"><strong><span style="color:rgb(255, 255, 255);font-size:12pt;"><a href="http://ampirtex37.ru/kontakty/" style="text-decoration:none;text-decoration-line:none;text-decoration-style:initial;text-decoration-color:initial;color:rgb(0, 0, 0);" rel="noopener noreferrer" target="_blank"><sub></sub>Контакты</a></span></strong></td></tr></tbody></table>
                                </td>
                            </tr>
                        	<tr>
                            	<td align="center" valign="top">
                                    <!-- Body -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
                                    	<tr>
                                            <td valign="top" id="body_content">
                                                <!-- Content -->
                                                <table border="0" cellpadding="20" cellspacing="0" width="100%">
                                                    <tr>
                                                        <td valign="top">
                                                            <div id="body_content_inner">
