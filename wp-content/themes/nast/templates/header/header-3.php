<?php
global $theme_option,$woocommerce;
$login_url = wp_login_url();
$register_url = wp_registration_url();
$account_link = get_edit_profile_url();
$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
if ( $myaccount_page_id ) {
    $login_url = get_permalink( $myaccount_page_id );
    if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' ) {
        $login_url = str_replace( 'http:', 'https:', $login_url );
    }
    if( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ){
        $register_url = $login_url;
    }
    $account_link = get_permalink($myaccount_page_id);
}
?>
<header id="pgl-header" class="pgl-header pgl-header3">
    <div id="header-topbar">
        <div class="container">
            <div class="inner-topbar row">
                <div class="col-sm-6 topbar-1">
                    <?php if(isset($theme_option['header_topbar_text']) && $theme_option['header_topbar_text']!='') echo $theme_option['header_topbar_text']; ?>
                    <?php if( isset($theme_option['header-is-switch-language']) && $theme_option['header-is-switch-language'] ){ ?>
                    <div class="language-filter pull-left">
                        <?php echo pgl_language_flags(); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="col-sm-6 topbar-2">
                    <div class="header-toplinks">
                        <ul class="links">
                            <?php if(is_user_logged_in()){ ?>
                                <li class="first">
                                    <a class="account" href="<?php echo esc_url($account_link); ?>" title="Account">
                                        <?php echo __('Account','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(PLG_WOOCOMMERCE_ACTIVED){ ?>
                                <?php if ( class_exists( 'YITH_WCWL_UI' ) ) { global $yith_wcwl; ?>
                                    <li>
                                        <a class="wishlist" href="<?php echo esc_url($yith_wcwl->get_wishlist_url()); ?>" title="<?php _e('Wishlist','nast'); ?>">
                                            <?php echo __('Wishlist','nast'); ?>
                                        </a>
                                    </li>
                                <?php } ?>
                                <li>
                                    <a class="checkout" href="<?php echo esc_url($woocommerce->cart->get_checkout_url()); ?>" title="Checkout">
                                        <?php echo __('Checkout','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                            <?php if(!is_user_logged_in()){ ?>
                                <li class="last">
                                    <a class="login" href="<?php echo esc_url($login_url); ?>" title="<?php _e('Log In','nast'); ?>">
                                        <?php echo __('Log In','nast'); ?>
                                    </a>
                                    <span>/</span>
                                    <a class="register" href="<?php echo esc_url($register_url); ?>" title="<?php _e('Customer Register','nast'); ?>">
                                        <?php echo __('Register','nast'); ?>
                                    </a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="header-content">
        <div class="container">
            <div class="header-content-inner">
                <div class="row">
                    <div class="col-sm-2">
                        <div class="logo">
                            <?php do_action('pgl_set_logo'); ?>
                        </div>
                    </div>
                    <div class="col-sm-10">
                        <?php if( PLG_WOOCOMMERCE_ACTIVED && $theme_option['header-is-cart'] ){ ?>
                            <div class="shoppingcart">
                                <a href="javascript:;" data-uk-offcanvas="{target:'#pgl_cart_canvas'}">
                                    <span class="count"><?php echo $woocommerce->cart->cart_contents_count; ?></span>
                                    <span class="text"><?php echo __('Shopping Bag','nast'); ?></span>
                                </a>
                            </div>
                        <?php } ?>
                        <?php if($theme_option['header-is-search']){ ?>
                            <div class="search-form">
                                <div class="icon-search"><i class="fa fa-search"></i></div>
                                <div class="box-search">
                                    <div class="box-search-info">
                                        <?php get_search_form(); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="nav-container">
        <div class="container">
            <div class="nav-inner">
                <div class="toggle-menu">
                    <a href="javascript:;" class="off-canvas-toggle icon-toggle" data-uk-offcanvas="{target:'#pgl-off-canvas'}">
                        <i class="fa fa-bars"></i>
                        <?php echo __('Menu','nast'); ?>
                    </a>
                </div>
                <?php pgl_megamenu(array(
                    'theme_location' => 'mainmenu',
                    'container_class' => 'collapse navbar-collapse navbar-ex1-collapse pull-left',
                    'menu_class' => 'nav navbar-nav megamenu',
                    'show_toggle' => false
                )); ?>
            </div>
        </div>
    </div>
    <?php do_action('pgl_after_header'); ?>
</header>
<!-- //HEADER -->