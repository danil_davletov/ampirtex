
<article id="post-<?php the_ID(); ?>" <?php (is_sticky()) ? post_class('blog-container stick_post') : post_class('blog-container'); ?>>
	
	<div class="blog-container-inner">
		<?php do_action('pgl_post_before_content'); ?>
		<h2 class="blog-title">
			<?php if(is_sticky()){ ?>
				<span class="sticky pull-right"><?php _e('Sticky','nast'); ?></span>
			<?php } ?>
			<a href="<?php the_permalink(); ?>">
				<?php the_title(); ?>
			</a>
		</h2>
		<?php // get_template_part( 'templates/single/meta' ); ?>
		<div class="blog-content">
			<?php echo pgl_get_excerpt(120); ?>
		</div>
		<a href="<?php the_permalink(); ?>" class="btn btn-default"><?php echo __( 'Подробнее', 'nast' ); ?></a>
	</div>
</article>