
<article id="post-<?php the_ID(); ?>" <?php post_class('blog-container blog-masonry col-sm-'.(12/apply_filters( 'pgl_blog_masonry_column', 3 )) ); ?>>
	<?php //get_template_part( 'templates/single/meta' ); ?>
	<div class="blog-container-inner">
		<?php do_action('pgl_post_before_content'); ?>
		<div class="masonry-inner">
			<header>
				<div class="meta-date pull-left">
					<span>
						<span class="text-center">
							<span class="d"><?php echo the_time( 'd' ); ?></span><br>
							<span class="my"><?php the_time( 'M Y' ); ?></span>
						</span>
					</span>
				</div>
				<div class="meta-heading">
					<h2 class="blog-title">
						<a href="<?php the_permalink(); ?>">
							<?php the_title(); ?>
						</a>
					</h2>
					<ul class="masonry-meta">
						<li class="meta-author">
							<?php _e('by','nast'); ?> <?php the_author_posts_link(); ?>
						</li>
						<li class="meta-category">
							<?php _e('in','nast'); ?> <?php the_category( ', ' ); ?>
						</li>
						<li class="meta-comment">
							<i class="fa fa-comment-o"></i>
							<?php comments_popup_link(' 0', ' 1', ' %'); ?>
						</li>
					</ul>
				</div>
			</header>
			<div class="blog-content">
				<?php echo pgl_get_excerpt(25,'...'); ?>
			</div>
		</div>
	</div>
</article>