<?php

extract( shortcode_atts( array(
	'title'=>'Testimonials',
	'el_class' => '',
	'skin'=>'skin-1'
), $atts ) );
$el_class = $this->getExtraClass($el_class);
$_id = pgl_make_id();
$_count = 0;
$args = array(
	'post_type' => 'testimonial',
	'posts_per_page' => -1,
	'post_status' => 'publish'
);

$query = new WP_Query($args);
?>

<div class="box pgl-testimonial <?php echo esc_attr($el_class); ?>">
	<?php if($query->have_posts()){ ?>
		<div id="carousel-<?php echo esc_attr($_id); ?>" class="inner-content post-widget media-post-carousel carousel slide" data-ride="carousel">
			<div class="carousel-inner testimonial-carousel">
			<?php while($query->have_posts()):$query->the_post(); ?>
				<!-- Wrapper for slides -->
				<div class="item<?php echo (($_count==0)?" active":"") ?>">
                    <div class="image pull-center">
                        <?php the_post_thumbnail( 'thumbnail' ); ?>
                    </div>
					<div class="std">
						<?php the_content(); ?>
					</div>
					<div class="post-by">
                        <span class="title"><?php the_title(); ?></span> -
                        <span class="sub-title">November 14, 2014</span>
					</div>
				</div>
				<?php $_count++; ?>
			<?php endwhile; ?>

			</div>
		</div>
	<?php } ?>
</div>
<?php wp_reset_postdata(); ?>