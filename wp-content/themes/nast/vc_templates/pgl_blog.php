<?php
$grid_link = $layout_mode = $title = $filter= '';
$posts = array();

extract(shortcode_atts(array(
    'title' => '',
    'columns_count' => 4,
    'layout' => 'grid',
    'grid_thumb_size' => 'thumbnail',
    'el_class' => '',
    'orderby' => NULL,
    'order' => 'DESC',
    'loop' => '',
), $atts));


if(empty($loop)) return;

$this->getLoop($loop);

$sticky = get_option('sticky_posts');
$this->loop_args['post__not_in'] = $sticky;

$my_query = new WP_Query($this->loop_args);

$column = 12/$columns_count;

?>
<div class="grid-posts grid-posts ">
    <div class="row">
        <?php if($layout=='carousel'){ ?>
            <div data-owl="slide" 
                    data-item-slide="<?php echo esc_attr($columns_count); ?>" 
                    data-ow-rtl="<?php echo is_rtl()?'true':'false'; ?>" 
                    class="owl-carousel owl-theme">
            <?php while ( $my_query->have_posts() ): $my_query->the_post();global $post; ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class('blog-container blog-visual col-sm-12'); ?>>
                    <?php get_template_part( 'templates/blog/blog-visual' ); ?>
                </article>
            <?php endwhile; ?>
            </div>
        <?php }else{ ?>
            <?php while ( $my_query->have_posts() ): $my_query->the_post();global $post; ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class('blog-container blog-visual col-sm-'.$column ); ?>>
                    <?php get_template_part( 'templates/blog/blog-visual' ); ?>
                </article>
            <?php endwhile; ?>
        <?php } ?>
    </div>
</div>
<?php
wp_reset_postdata();


