<?php

define( 'PGL_THEME_DIR', get_template_directory() );
define( 'PGL_THEME_URI', get_template_directory_uri() );

define( 'PGL_FRAMEWORK_PATH', PGL_THEME_DIR . '/framework/' );
define( 'PGL_FRAMEWORK_URI', PGL_THEME_URI . '/framework/' );

define( 'PLG_REDUX_FRAMEWORK_ACTIVED', in_array( 'redux-framework/redux-framework.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );
define( 'PLG_WOOCOMMERCE_ACTIVED', in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) );


if(!PLG_REDUX_FRAMEWORK_ACTIVED){
	require_once( PGL_THEME_DIR.'/framework/redux-framework/redux-framework.php' );
}

require_once( PGL_THEME_DIR.'/framework/metabox/meta-item.php'  );
require_once( PGL_THEME_DIR.'/framework/admin/options.php' );
// require_once( PGL_THEME_DIR.'/framework/admin/multiple_sidebars.php' );
require_once( PGL_THEME_DIR.'/framework/admin/plugin-activation.php' );
require_once( PGL_THEME_DIR.'/framework/framework.php' );
require_once( PGL_THEME_DIR.'/framework/front/function.php' );

require_once( PGL_THEME_DIR.'/framework/customize/init.php' );
// require_once( PGL_THEME_DIR.'/framework/customize/front.php' );

require_once( PGL_THEME_DIR.'/framework/megamenu/megamenu.php' );



/*==========================================================================
Woocommerce
==========================================================================*/
if(PLG_WOOCOMMERCE_ACTIVED){
	require_once( PGL_THEME_DIR.'/framework/woocommerce/woocommerce.php' );
}
